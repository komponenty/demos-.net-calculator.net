﻿namespace Calculator.Core.Contract
{
    public interface IMemoryOperations
	{
		void MemoryClear();
		double MemoryRetrive();
		void MemoryAdd(double number);
        void MemorySub(double number);
      
		void Reset();
	}
}
