﻿using Calculator.ALU.Contract;
using System.Collections.Generic;

namespace Calculator.Core.Contract
{
    public interface ICalculations
    {
        IEnumerable<IOperation> AllowedOperations { get; }

        void PerformOperation(IOperation operation, double number);

        double CurrentValue { get; }

        void Reset();
    }
}
