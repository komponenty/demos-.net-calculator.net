﻿using Calculator.UI.Display.Contract;
using System.Windows.Controls;

namespace Calculator.UI.Display
{
    public class Display : IDisplay
    {
        private DisplayControl control;
        private DisplayViewModel model;

        public Display()
        {
            control = new DisplayControl();
            model = new DisplayViewModel();
            control.DataContext = model;
        }

        string IDisplay.Value
        {
            get { return model.Text; }
            set { model.Text = value; }
        }

        void IDisplay.Clear()
        {
            model.Text = "";
        }

        Control IDisplay.Control
        {
            get { return control; }
        }


        bool IDisplay.IsEnabled
        {
            get
            {
                return model.IsEnabled;
            }
            set
            {
                model.IsEnabled = value; ;
            }
        }
    }
}
