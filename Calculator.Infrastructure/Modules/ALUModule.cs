﻿using Calculator.ALU.Contract;
using Ninject.Extensions.Conventions;
using Ninject.Modules;

namespace Calculator.Infrastructure.Modules
{
    class ALUModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(x => x
                //.FromAssembliesMatching("*")
                .FromAssembliesInPath(".")
                .IncludingNonePublicTypes()
                .SelectAllClasses()
                .InheritedFrom<IOperation>()
                .BindAllInterfaces()
                .Configure(b => b.InSingletonScope()));
        }
    }
}
