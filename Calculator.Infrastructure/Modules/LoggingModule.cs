﻿using Ninject.Modules;
using System;
using Common.Logging;

namespace Calculator.Infrastructure.Modules
{
    class LoggingModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILog>().ToMethod(x => LogManager.GetLogger(x.Request.ParentRequest.Service.FullName));
        }
    }
}
