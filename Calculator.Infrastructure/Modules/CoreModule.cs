﻿using Calculator.Core.Contract;
using Calculator.Core.Impl;
using Ninject;
using Ninject.Modules;

namespace Calculator.Infrastructure.Modules
{
    class CoreModule : NinjectModule
    {
        public override void Load()
        {
            Bind<Device>().ToSelf().InSingletonScope();
            Bind<ICalculations>().ToMethod(ctx => ctx.Kernel.Get<Device>().GetCalculations());
            Bind<IMemoryOperations>().ToMethod(ctx => ctx.Kernel.Get<Device>().GetMemoryOperations());
        }
    }
}
