﻿using Calculator.Memory;
using Calculator.Memory.Contract;
using Ninject.Modules;

namespace Calculator.Infrastructure.Modules
{
    class MemoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMemoryManager>().To<MemoryManager>().InSingletonScope();
        }
    }
}
