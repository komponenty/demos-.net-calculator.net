﻿using Calculator.ALU.Contract;
using Common.Logging;


namespace Calculator.ALU.Basic
{
    abstract class Operation : IOperation
    {
        private readonly ILog logger;
        private readonly string name;
        private readonly string symbol;

        string IOperation.Name
        {
            get { return name; }
        }

        string IOperation.Symbol
        {
            get { return symbol; }
        }

        protected Operation(string name, string symbol, ILog logger)
        {
            this.name = name;
            this.symbol = symbol;
            this.logger = logger;
        }

        double IOperation.Calculate(double number1, double number2)
        {
            double result = calculate(number1, number2);
            logger.Info(string.Format("{0} {1} {2} = {3}", number1, symbol, number2, result));
            return result;
        }

        protected abstract double calculate(double number1, double number2);
        
    }

    class Add : Operation
    {
        public Add(ILog logger) : base("Add", "+", logger) {}

        protected override double calculate(double number1, double number2)
        {
            return number1 + number2;
        }
    }

    class Sub : Operation
    {
        public Sub(ILog logger) : base("Sub", "-", logger) {}

        protected override double calculate(double number1, double number2)
        {
            return number1 - number2;
        }
    }

    class Mul : Operation
    {
        public Mul(ILog logger) : base("Mul", "×", logger) { }

        protected override double calculate(double number1, double number2)
        {
            return number1 * number2;
        }
    }

    class Div : Operation
    {
        public Div(ILog logger) : base("Div", "÷", logger) { }
        
        protected override double calculate(double number1, double number2)
        {
            return number1 / number2;
        }
    }
}
