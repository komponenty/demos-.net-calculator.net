﻿using System.Windows.Controls;

namespace Calculator.UI.Display.Contract
{
    public interface IDisplay
    {
        string Value { get; set; }
        void Clear();

        bool IsEnabled { get; set; }

        Control Control { get; }
    }
}
