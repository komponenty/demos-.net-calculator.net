﻿namespace Calculator.Memory.Contract
{
    public interface IMemoryManager
    {
        IRegister<T> CreateRegister<T>(string name);
        IRegister<T> GetRegister<T>(string name);
        IRegister<T> GetOrCreateRegister<T>(string name);
    }
}
