﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Memory.Contract
{
    public interface IRegister 
    {
        void Clear();
    }

    public interface IRegister<T> : IRegister
    {
        T Retrive();
        void Store(T value);
    }
}
