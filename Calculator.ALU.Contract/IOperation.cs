﻿
namespace Calculator.ALU.Contract
{
    public interface IOperation
    {
        string Name { get; }
        string Symbol { get; }
        double Calculate(double number1, double number2);
    }
}
