﻿using System;
using System.Collections.Generic;

namespace Calculator.ALU.Contract
{
    public interface IALU
    {
        IEnumerable<IOperation> AllowedOperations { get; }
    }
}
