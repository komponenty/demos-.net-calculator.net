﻿using Calculator.Core.Contract;
using Calculator.Memory.Contract;
using Common.Logging;

namespace Calculator.Core.Impl
{
    class MemoryOperations : IMemoryOperations
    {
        private IRegister<double> memory;
        private IRegister<double> accumulator;

        private readonly ILog logger;

        public MemoryOperations(IRegister<double> memory, IRegister<double> accumulator, ILog logger)
        {
            this.memory = memory;
            this.accumulator = accumulator;

            this.logger = logger;
        }

        public double MemoryRetrive() 
        { 
            double result = memory.Retrive();
            logger.Info(string.Format("Retrive from memory value {0}", result));
            return result;
        }

        public void MemoryAdd(double number) 
        {
            logger.Info(string.Format("Add to memory value {0}", number));
            memory.Store(memory.Retrive() + number); 
        }

        public void MemorySub(double number)
        {
            logger.Info(string.Format("Subtract from memory value {0}", number));
            memory.Store(memory.Retrive() - number);
        }

        public void MemoryClear() 
        {
            logger.Info("Memory clear");
            memory.Clear(); 
        }

        public void Reset()
        {
            logger.Info("Memory reset");
            accumulator.Clear();
            memory.Clear();
        }
    }
}
