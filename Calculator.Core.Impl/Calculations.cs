﻿using Calculator.Core.Contract;
using Calculator.ALU.Contract;
using Calculator.Memory.Contract;
using System.Collections.Generic;
using Common.Logging;

namespace Calculator.Core.Impl
{
    class Calculations : ICalculations
	{
        private List<IOperation> allowedOperations;
        private IRegister<double> accumulator;
        private IRegister<IOperation> lastOperation;

        private readonly ILog logger;

        public Calculations(IEnumerable<IOperation> allowedOperations, IRegister<double> accumulator, IRegister<IOperation> lastOperation, ILog logger)
        {
            this.allowedOperations = new List<IOperation>(allowedOperations);
            this.accumulator = accumulator;
            this.lastOperation = lastOperation;

            this.logger = logger;
        }

		IEnumerable<IOperation> ICalculations.AllowedOperations 
		{
            get { return allowedOperations.AsReadOnly(); } 
		}

        void ICalculations.PerformOperation(IOperation operation, double number)
		{
            logger.Info(string.Format("Perform operation {0} with number {1}", (operation != null) ? operation.Name : "null", number));
            if (operation == null)
                operation = lastOperation.Retrive();
            else
                lastOperation.Store(operation);
            double result = (operation == null) ? number : operation.Calculate(accumulator.Retrive(), number);
            accumulator.Store(result);
		}

        double ICalculations.CurrentValue
        {
            get 
            { 
                double result = accumulator.Retrive();
                logger.Info(string.Format("Current value is {0}", result));
                return result;
            }
        }

        void ICalculations.Reset()
        {
            logger.Info("Calculations reset");
            accumulator.Clear();
            lastOperation.Clear();
        }
	}
}
