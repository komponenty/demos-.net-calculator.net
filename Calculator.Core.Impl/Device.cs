﻿using Calculator.ALU.Contract;
using Calculator.Core.Contract;
using Calculator.Memory.Contract;
using Common.Logging;
using System.Collections.Generic;

namespace Calculator.Core.Impl
{
    public class Device
    {
        private ICalculations calculations;
        private IMemoryOperations memoryOperations;

        public Device(IEnumerable<IOperation> allowedOperations, IMemoryManager memoryManager, ILog logger)
        {
            var accumulator = memoryManager.GetOrCreateRegister<double>("Accumulator");
            var lastOperation = memoryManager.GetOrCreateRegister<IOperation>("LastOperation");
            var memory = memoryManager.GetOrCreateRegister<double>("InternalMemory");
            calculations = new Calculations(allowedOperations, accumulator, lastOperation, logger);
            memoryOperations = new MemoryOperations(memory, accumulator, logger);
        }

        public ICalculations GetCalculations() { return calculations; }

        public IMemoryOperations GetMemoryOperations() { return memoryOperations; }
    }
}
