﻿using Calculator.Memory.Contract;
using Common.Logging;
using System;
using System.Collections.Generic;

namespace Calculator.Memory
{
    public class MemoryManager : IMemoryManager
    {
        private IDictionary<string, IRegister> registers;

        private readonly ILog logger;

        public MemoryManager(ILog logger)
        {
            registers = new Dictionary<string, IRegister>();

            this.logger = logger;
        }

        IRegister<T> IMemoryManager.CreateRegister<T>(string name)
        {
            if (registers.ContainsKey(name))
                throw new ArgumentException(string.Format("Register with name {0} already existing", name));
            logger.Info(string.Format("Create new register \"{0}\"", name));
            IRegister register = new Register<T>(logger);
            registers[name] = register;
            return register as IRegister<T>;
        }

        IRegister<T> IMemoryManager.GetRegister<T>(string name)
        {
            logger.Info(string.Format("Get register \"{0}\"", name));
            return registers[name] as IRegister<T>;
        }

        IRegister<T> IMemoryManager.GetOrCreateRegister<T>(string name)
        {
            IMemoryManager mm = this;
            try
            {
                return mm.GetRegister<T>(name);
            }
            catch (Exception ex)
            {
                return mm.CreateRegister<T>(name);
            }
        }
    }
}
