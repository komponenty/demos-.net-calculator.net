﻿using Calculator.Memory.Contract;
using Common.Logging;

namespace Calculator.Memory
{
    class Register<T> : IRegister<T>
    {
        T value;

        private readonly ILog logger;

        public Register(ILog logger)
        {
            this.logger = logger;

            Clear();
        }

        public T Retrive()
        {
            logger.Info(string.Format("Retrive register value: {0}", value));
            return value;
        }

        public void Store(T value)
        {
            logger.Info(string.Format("Store register value: {0}", value));
            this.value = value;
        }

        public void Clear()
        {
            logger.Info("Clear register");
            value = default(T);
        }
    }
}
