﻿using System;

namespace Calculator.UI.Keyboard.Contract
{
    public class KeyboardEventArgs : EventArgs
    {
        public string Symbol { get; private set; }

        public KeyboardEventArgs(string symbol)
        {
            this.Symbol = symbol;
        }
    }
}
