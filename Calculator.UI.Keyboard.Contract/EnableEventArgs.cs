﻿using System;

namespace Calculator.UI.Keyboard.Contract
{
    public class EnableEventArgs : EventArgs
    {
        public bool IsEnabled { get; private set; }

        public EnableEventArgs(bool enabled)
        {
            this.IsEnabled = enabled;
        }
    }
}
